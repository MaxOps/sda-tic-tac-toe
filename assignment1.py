"""
Create a class Students that holds students record and activities
The class must have a constructor
*At least 3 methods
*Attempt to Write tests for your class
*Push your code to git repository
**Create a branch called dev and push your code into branch
**Ensure you have a gitignore file - gitignore.io
**Ensure commit statements are readable
**Have a requirements.txt file
"""


class Students:

    def __init__(self, first_name, course, gpa):
        self.__first_name = first_name
        self.__course = course
        self.__gpa = gpa

    def getfirst_name(self):
        return self.__first_name

    def getcourse(self):
        return self.__course

    def getgpa(self):
        return self.__gpa